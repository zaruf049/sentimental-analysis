# Sentimental Analysis

Sentiment analysis is the task of identifying whether the opinion expressed in a text is positive or negative in general. It is a kind of data mining where you measure the inclination of people's opinions by using NLP. Sentiment analysis is often performed on textual data to help businesses monitor brand and product sentiment in customer feedback, and understand customer needs.

This project analyses everyday language that we use in communication and produces number of tokens in the language,keywords,polarity and subjectivity of the context.
